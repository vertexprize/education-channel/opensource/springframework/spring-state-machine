package org.vertexprize.education.spring.statemachine;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.statemachine.StateMachine;
import org.vertexprize.education.spring.statemachine.config.StateMachineConfig.Events;
import org.vertexprize.education.spring.statemachine.config.StateMachineConfig.States;

@SpringBootApplication
@Slf4j
public class SpringStateMachineApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringStateMachineApplication.class, args);
    }

    @Autowired
    private StateMachine<States, Events> stateMachine;
    
    
    
    @PostConstruct
    public void start() {
        log.info("Старт машины состостояний ...");

        log.info ("Отправка события ["+Events.E1+"] ");
        stateMachine.sendEvent(Events.E1);
        
        log.info ("Отправка события ["+Events.E2+"] ");
        stateMachine.sendEvent(Events.E2);
               
    }
    
    @PreDestroy
    public void stop() {
        log.info("Остановка  машины состостояний ...");

       
    }
    

}
